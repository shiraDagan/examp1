// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //נקח את התוכן מהפיירבס נלחץ על האפליקציה, הגדרות, <>, נעתיק את שם האפליקציה ונאשר
   firebaseConfig : {
    apiKey: "AIzaSyCDDfecoePaPwMlh5z0bBWTdDZQfKpWis0",
    authDomain: "shira-s-angular-app.firebaseapp.com",
    databaseURL: "https://shira-s-angular-app.firebaseio.com",
    projectId: "shira-s-angular-app",
    storageBucket: "shira-s-angular-app.appspot.com",
    messagingSenderId: "159662927267",
    appId: "1:159662927267:web:f6b840600e01622257be16",
    measurementId: "G-F1XJN8N3JK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
