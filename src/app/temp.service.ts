// import { WeatherRaw } from './interfaces/weather-raw';
// import { Observable, throwError } from 'rxjs';//ADD
// import { Injectable } from '@angular/core';
// import { HttpClient, HttpErrorResponse } from '@angular/common/http';//ADD
// import { Weather } from './interfaces/weather';
// import { map ,catchError} from 'rxjs/operators';



// @Injectable({
//   providedIn: 'root'
// })
// export class TempService {
  
// private URL = "http://api.openweathermap.org/data/2.5/weather?q=";
// private KEY =  "2ceb9ee9c80e3bfeef96a7c1b52f2ade";
// private IMP = "&units=metric";

// //api יראה שגיאה ונרצה להציג אותה למשתמש
// searchWeatherData(cityName:String):Observable<Weather>{
//   return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
//   //pipe מקבלת לתוכה אוסף של פונקציות
//   .pipe(
//     map (data=>this.transformWeatherData(data)),//map לוקחת את הנתונים הגולמיים ומכניסה אותם לדטה. היא הפונקציה שמבצעת את ההמרה
// catchError(this.handleError)

//     )
// }
// //HttpErrorResponse מחלקה שמיועדת לקלוט שגיאות
// private handleError(res:HttpErrorResponse){//שיטה פרטית לא ניתן לקרוא לה מבחוץ
// console.log(res.error);
// return throwError(res.error)
// }



// private transformWeatherData(data:WeatherRaw):Weather{
//   return{
//     name:data.name,
//     country:data.sys.country,
//     image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
//     description: data.weather[0].description,
//     temperature: data.main.temp,
//     lat:data.coord.lat,
//     lon:data.coord.lon



//   }

// }
//   constructor(private http:HttpClient) { }//add
// }
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';
//import { WeatherData } from './../shared/interfaces/weather-data';
//import { Weather } from './../shared/interfaces/weather';

import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "c2addb75777e24415bc0705646e8a194";
  private IMP = "&units=metric";

 searchWeatherData(cityName:string): Observable<Weather> {
   return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
      .pipe(
        map(data => this.transformWeatherData(data)),
        catchError(this.handleError)
        
      )
 }
private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error)
    
}

/*
searchWeatherData(cityName:string): Observable<Weather> {
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
                    .pipe(
                      map(data => this.transformWeatherData(data)),
                      catchError(this.handleError),
                      tap(data =>console.log(JSON.stringify(data))),

                    )  
 }
*/

 constructor(private http: HttpClient) { }
  
/*
 private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return throwError(res.error || 'Server error');
  }
*/
  private transformWeatherData(data:WeatherRaw):Weather {
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature: data.main.temp,
      lat: data.coord.lat,
      lon: data.coord.lon
    }
  }

}
