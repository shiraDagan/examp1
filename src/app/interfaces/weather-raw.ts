export interface WeatherRaw {
  //נבחר את הפרמטרים שרלוונטיים לנו לאפליקציה
  weather: [//משתנה שהוא בעצמו מערך
    {
     
     description: String
      icon: String
    }
  ];
  main: {
    temp: number,
  };
 
  sys: {
    // country: String,
    country: "GB",//eliran
    
  };
 
   coord:{//אם נרצה בעתיד להראות עיר על מפה נצטרך להשתמש ב lon וlat
       lon:number,
       lat:number
   }
   name:String
}
