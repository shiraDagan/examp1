// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-tempform',
//   templateUrl: './tempform.component.html',
//   styleUrls: ['./tempform.component.css']
// })
// export class TempformComponent implements OnInit {

//   cities:object[] = [{id:1, name:'non existing'},{id:2, name:'London'},{id:3, name:'Paris'}];
//   temperature:number;
//   city:string; 

//   onSubmit(){
//     this.router.navigate(['/temperatures', this.city]);
//   }
  
//   constructor(private router: Router) { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cities:object[] = [{id:1, name:'non existing'},{id:2, name:'London'},{id:3, name:'Paris'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperatures', this.city]);
  }
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

}
