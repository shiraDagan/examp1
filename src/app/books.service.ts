import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  //db:any;
  // getBooks(){
  //   const booksObservable = new Observable(
  //     observer => {
  //       setInterval(
  //        ()=> observer.next(this.books),5000
  //       )
  //     }
  //   )
  //   return booksObservable;
  // }
  getBooks():Observable<any[]>{//any זה עיגול פינות מכיוון שבמקום זה צריך להגדיר אינטרפייס של אובייקט במקרה שלנו בוקס
 return this.db.collection('books').valueChanges();
  }

  addBooks(title:string, author:string){
  const book = {title:title, author:author};
  this.db.collection('books').add(book);
  }
  // addBooks(){
  //   setInterval(
  //   ()=> this.books.push ({title:'A new book', author:'New author'}),
  //   5000)
   
 // }

  // getBooks(){
  //   setInterval(()=>this.books,1000)

  // }

  constructor(private db:AngularFirestore) { }//תיצור אובייקט מסוג אנגולר פיירסטור ותכניס אותו ל db
}
