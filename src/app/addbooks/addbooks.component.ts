import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from './../books.service';
@Component({
  selector: 'app-addbooks',
  templateUrl: './addbooks.component.html',
  styleUrls: ['./addbooks.component.css']
})
export class AddbooksComponent implements OnInit {
title;
author;

  constructor(private router: Router, private bookservice:BooksService) { }

  onSubmit() {
  this.bookservice.addBooks(this.title,this.author);
 this.router.navigate(['/books']);
  }
  ngOnInit() {
  }
}
